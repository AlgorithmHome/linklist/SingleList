
public class Main {

    public static void main(String[] args) {
        ListNode head = new ListNode(2);
        ListNode temp = head;
        for (int i=0;i<10;i++){
            temp.next = new ListNode(4+i);
            temp = temp.next;
        }
        ListNode tempHead = head;
        while (tempHead!=null){
            System.out.println(tempHead.val);
            tempHead = tempHead.next;
        }
    }
}